# Node API Design

### Basic Express Server
* npm init
* install express, dotenv
* install nodemon as dev dependencie
* setup scripts
  * "start": "NODE_ENV=production node server",
  * "dev": nodemon server

### Routes and Response
* set http status for each response
* route structure get/post/put/delete
  * versioning the API make it updateable without affecting client 
  *  /api/v1/ressource
* access rrquests parameter from url(:id) with req.params.id

### Routes
* require express
* init router, router = express.Router()
* Router.get/post/put/delete, export router
* link route to file
* app.use('url', file)

### Controller Methods
* mkdir controllers
  * file for each router
  * middleware functions (req, res, next)
  * add description header for each function
  * //@desc   // Get all users
  * //@route  // GET /api/vi/users
  * //@access // Pubic
* import controller in routes {each controller function}
* router.route('/').get(getUsers).post(postUsers)


### Middlware Basics
* all middleware function take req, res, next
* 
